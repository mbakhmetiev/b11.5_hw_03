#### :one: Требуется настроить пайплайн, который будет запускаться при изменении SQL-скрипта, хранящегося в репозитории (его выбор также на ваше усмотрение). 

> #### конфиг пайплайна:
```
---
image: mysql

get the sql queries:
  script:
    -  bash sqlscript.sh
  artifacts:
    paths:
    - ./sqloutput
```

> #### `sql` запросы я вставил в `bash` скрипт:
```bash
#!/usr/bin/bash

# create directory to store the queries

outdir="sqloutput"
outfile="sqlqueries"
mkdir $outdir

# run query 1

mysql --user rfamro --host mysql-rfam-public.ebi.ac.uk --port 4497 --database Rfam -e 'SELECT * from fr.fram_acc limit 1;'  >> ./$outdir/$outfile

# run query 2

mysql --user rfamro --host mysql-rfam-public.ebi.ac.uk --port 4497 --database Rfam -e 'SELECT fr.rfam_acc, fr.rfamseq_acc, fr.seq_start, fr.seq_end FROM full_region fr, rfamseq rf, taxonomy tx WHERE rf.ncbi_id = tx.ncbi_id
AND fr.rfamseq_acc = rf.rfamseq_acc AND tx.ncbi_id = 10116  AND is_significant = 1 ' >> ./$outdir/$outfile
```
#### :two: Пайплайн должен запускать изменившийся скрипт и выводить результат запроса.

> #### я сохраняю результат `sql` запроса в артефакт, он доступен [здесь](https://gitlab.com/mbakhmetiev/b11.5_hw_03/-/jobs/4580093633/artifacts/file/sqloutput/sqlqueries)  
> #### должен признаться, что у меня не проходит первый `sql` запрос с ошибкой:  
```
ERROR 1142 (42000) at line 1: SELECT command denied to user 'rfamro'@'35.227.92.226' for table 'fram_acc'
```
> #### спишем это на косяк на стороне БД? второй проходит норм и записывает в артефакт




