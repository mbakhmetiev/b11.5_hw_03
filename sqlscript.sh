#!/usr/bin/bash

# create directory to store the queries

outdir="sqloutput"
outfile="sqlqueries"
mkdir $outdir

# run query 1

mysql --user rfamro --host mysql-rfam-public.ebi.ac.uk --port 4497 --database Rfam -e 'SELECT * from fr.fram_acc limit 1;'  >> ./$outdir/$outfile

# run query 2

mysql --user rfamro --host mysql-rfam-public.ebi.ac.uk --port 4497 --database Rfam -e 'SELECT fr.rfam_acc, fr.rfamseq_acc, fr.seq_start, fr.seq_end FROM full_region fr, rfamseq rf, taxonomy tx WHERE rf.ncbi_id = tx.ncbi_id
AND fr.rfamseq_acc = rf.rfamseq_acc AND tx.ncbi_id = 10116  AND is_significant = 1 ' >> ./$outdir/$outfile
